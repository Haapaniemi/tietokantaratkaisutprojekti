package tuholaistorjuntasovellus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Asiakas.class)
public abstract class Asiakas_ {

	public static volatile SingularAttribute<Asiakas, String> katuosoite;
	public static volatile SingularAttribute<Asiakas, String> sahkoposti;
	public static volatile SingularAttribute<Asiakas, Integer> asiakasId;
	public static volatile SingularAttribute<Asiakas, String> etunimi;
	public static volatile SingularAttribute<Asiakas, String> postinumero;
	public static volatile SingularAttribute<Asiakas, String> sukunimi;
	public static volatile SingularAttribute<Asiakas, String> puhelinnumero;

}


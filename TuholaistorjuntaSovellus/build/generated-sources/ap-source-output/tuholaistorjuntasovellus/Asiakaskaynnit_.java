package tuholaistorjuntasovellus;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Asiakaskaynnit.class)
public abstract class Asiakaskaynnit_ {

	public static volatile SingularAttribute<Asiakaskaynnit, Integer> tyontekijaId;
	public static volatile SingularAttribute<Asiakaskaynnit, Integer> tuholaisetId;
	public static volatile SingularAttribute<Asiakaskaynnit, Integer> asiakasId;
	public static volatile SingularAttribute<Asiakaskaynnit, String> tila;
	public static volatile SingularAttribute<Asiakaskaynnit, Integer> asiakaskayntiId;
	public static volatile SingularAttribute<Asiakaskaynnit, Timestamp> aikaleima;

}


package tuholaistorjuntasovellus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Loytyneettuholaiset.class)
public abstract class Loytyneettuholaiset_ {

	public static volatile SingularAttribute<Loytyneettuholaiset, Integer> tuholaisetId;
	public static volatile SingularAttribute<Loytyneettuholaiset, Integer> tuholainenId;

}


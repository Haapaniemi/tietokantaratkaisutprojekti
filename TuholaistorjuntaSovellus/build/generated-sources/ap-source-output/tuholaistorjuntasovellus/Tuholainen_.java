package tuholaistorjuntasovellus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tuholainen.class)
public abstract class Tuholainen_ {

	public static volatile SingularAttribute<Tuholainen, String> nimi;
	public static volatile SingularAttribute<Tuholainen, String> torjunta;
	public static volatile SingularAttribute<Tuholainen, Integer> tuholainenId;
	public static volatile SingularAttribute<Tuholainen, String> kuvaus;

}


package tuholaistorjuntasovellus;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tyontekija.class)
public abstract class Tyontekija_ {

	public static volatile SingularAttribute<Tyontekija, Integer> tyontekijaId;
	public static volatile SingularAttribute<Tyontekija, String> sahkoposti;
	public static volatile SingularAttribute<Tyontekija, String> katuosoite;
	public static volatile SingularAttribute<Tyontekija, String> etunimi;
	public static volatile SingularAttribute<Tyontekija, String> postinumero;
	public static volatile SingularAttribute<Tyontekija, String> sukunimi;
	public static volatile SingularAttribute<Tyontekija, String> puhelinnumero;

}


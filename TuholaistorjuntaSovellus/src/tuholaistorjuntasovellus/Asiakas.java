/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuholaistorjuntasovellus;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author CleverName

*/
@Entity
@NamedQuery(name="Asiakas.getAll", query="SELECT c FROM Asiakas c")
public class Asiakas implements Serializable {
   
    private int AsiakasId; 
    private String Etunimi;
    private String Sukunimi;
    private String Puhelinnumero;
    private String Katuosoite;
    private String Postinumero;
    private String Sahkoposti;
    
    public Asiakas() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getAsiakasId() {
        return AsiakasId;
    }

    public void setAsiakasId(int id) {
        this.AsiakasId = id;
    }
    @Column
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String etunimi) {
        this.Etunimi = etunimi;
    }
    @Column
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String sukunimi) {
        this.Sukunimi = sukunimi;
    }
    @Column
    public String getPuhelinnumero() {
        return Puhelinnumero;
    }

    public void setPuhelinnumero(String puhelinnumero) {
        this.Puhelinnumero = puhelinnumero;
    }
    @Column
    public String getKatuosoite() {
        return Katuosoite;
    }

    public void setKatuosoite(String katuosoite) {
        this.Katuosoite = katuosoite;
    }
    @Column
    public String getPostinumero() {
        return Postinumero;
    }

    public void setPostinumero(String postinumero) {
        this.Postinumero = postinumero;
    }

    @Override
    public String toString() {
        return "Asiakas{" + "AsiakasId=" + AsiakasId + ", Etunimi=" + Etunimi + ", Sukunimi=" + Sukunimi + ", Puhelinnumero=" + Puhelinnumero + ", Katuosoite=" + Katuosoite + ", Postinumero=" + Postinumero + ", Sahkoposti=" + Sahkoposti + '}';
    }
    @Column
    public String getSahkoposti() {
        return Sahkoposti;
    }

    public void setSahkoposti(String sahkoposti) {
        this.Sahkoposti = sahkoposti;
    }

}

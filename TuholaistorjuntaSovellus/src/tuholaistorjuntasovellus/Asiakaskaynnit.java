/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuholaistorjuntasovellus;

import static com.oracle.jrockit.jfr.ContentType.Timestamp;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author CleverName
 */
@Entity
@NamedQuery(name="Asiakaskaynnit.getAll", query="SELECT c FROM Asiakaskaynnit c")
public class Asiakaskaynnit implements Serializable {
    
    private int AsiakaskayntiId;
    private int AsiakasId;
    private int TyontekijaId;
    private Timestamp Aikaleima;
    private String Tila;
    private int TuholaisetId;
    
    public Asiakaskaynnit() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getAsiakaskayntiId() {
        return AsiakaskayntiId;
    }

    public void setAsiakaskayntiId(int AsiakaskayntiId) {
        this.AsiakaskayntiId = AsiakaskayntiId;
    }
    @Column
    public int getAsiakasId() {
        return AsiakasId;
    }

    public void setAsiakasId(int AsiakasId) {
        this.AsiakasId = AsiakasId;
    }
    @Column
    public int getTyontekijaId() {
        return TyontekijaId;
    }

    public void setTyontekijaId(int TyontekijaId) {
        this.TyontekijaId = TyontekijaId;
    }
    @Column
    public Timestamp getAikaleima() {
        return Aikaleima;
    }

    public void setAikaleima(Timestamp Aikaleima) {
        this.Aikaleima = Aikaleima;
    }
    @Column
    public String getTila() {
        return Tila;
    }

    public void setTila(String Tila) {
        this.Tila = Tila;
    }
    @Column
    public int getTuholaisetId() {
        return TuholaisetId;
    }

    public void setTuholaisetId(int TuholaisetId) {
        this.TuholaisetId = TuholaisetId;
    }

    @Override
    public String toString() {
        return "Asiakaskaynnit{" + "AsiakaskayntiId=" + AsiakaskayntiId + ", AsiakasId=" + AsiakasId + ", TyontekijaId=" + TyontekijaId + ", Aikaleima=" + Aikaleima + ", Tila=" + Tila + ", TuholaisetId=" + TuholaisetId + '}';
    }
    
    
    
}

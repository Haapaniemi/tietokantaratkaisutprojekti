/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuholaistorjuntasovellus;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author CleverName
 */
@Entity
@NamedQuery(name="Loytyneettuholaiset.getAll", query="SELECT c FROM Loytyneettuholaiset c")
public class Loytyneettuholaiset implements Serializable {
    
    private int TuholaisetId;
    private int TuholainenId;
    
    public Loytyneettuholaiset() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getTuholaisetId() {
        return TuholaisetId;
    }

    public void setTuholaisetId(int TuholaisetId) {
        this.TuholaisetId = TuholaisetId;
    }
    @Column
    public int getTuholainenId() {
        return TuholainenId;
    }

    public void setTuholainenId(int TuholainenId) {
        this.TuholainenId = TuholainenId;
    }

    @Override
    public String toString() {
        return "Loytyneettuholaiset{" + "TuholaisetId=" + TuholaisetId + ", TuholainenId=" + TuholainenId + '}';
    }
    
    
    
}

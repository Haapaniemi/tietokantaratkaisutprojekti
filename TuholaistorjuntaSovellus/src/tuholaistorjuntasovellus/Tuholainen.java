/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuholaistorjuntasovellus;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author CleverName
 */
@Entity
@NamedQuery(name="Tuholainen.getAll", query="SELECT c FROM Tuholainen c")
public class Tuholainen implements Serializable {
    
    private int TuholainenId;
    private String Nimi;
    private String Kuvaus;
    private String Torjunta;
    
    public Tuholainen() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getTuholainenId() {
        return TuholainenId;
    }

    public void setTuholainenId(int TuholainenId) {
        this.TuholainenId = TuholainenId;
    }

    @Column
    public String getNimi() {
        return Nimi;
    }

    public void setNimi(String Nimi) {
        this.Nimi = Nimi;
    }
    @Column
    public String getKuvaus() {
        return Kuvaus;
    }

    public void setKuvaus(String Kuvaus) {
        this.Kuvaus = Kuvaus;
    }
    @Column
    public String getTorjunta() {
        return Torjunta;
    }

    public void setTorjunta(String Torjunta) {
        this.Torjunta = Torjunta;
    }

    @Override
    public String toString() {
        return "Tuholainen{" + "TuholainenId=" + TuholainenId + ", Nimi=" + Nimi + ", Kuvaus=" + Kuvaus + ", Torjunta=" + Torjunta + '}';
    }
    
    
    
    
}

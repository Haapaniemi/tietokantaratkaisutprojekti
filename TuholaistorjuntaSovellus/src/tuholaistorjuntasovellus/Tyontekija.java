/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuholaistorjuntasovellus;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author CleverName
 */
@Entity
@NamedQuery(name="Tyontekija.getAll", query="SELECT c FROM Tyontekija c")
public class Tyontekija implements Serializable {
    
    private int TyontekijaId;
    private String Etunimi;
    private String Sukunimi;
    private String Puhelinnumero;
    private String Sahkoposti;
    private String Katuosoite;
    private String Postinumero;
    
    public Tyontekija() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getTyontekijaId() {
        return TyontekijaId;
    }

    public void setTyontekijaId(int TyontekijaId) {
        this.TyontekijaId = TyontekijaId;
    }

    @Column
    public String getEtunimi() {
        return Etunimi;
    }

    public void setEtunimi(String Etunimi) {
        this.Etunimi = Etunimi;
    }
    @Column
    public String getSukunimi() {
        return Sukunimi;
    }

    public void setSukunimi(String Sukunimi) {
        this.Sukunimi = Sukunimi;
    }
    @Column
    public String getPuhelinnumero() {
        return Puhelinnumero;
    }

    public void setPuhelinnumero(String Puhelinnumero) {
        this.Puhelinnumero = Puhelinnumero;
    }
    @Column
    public String getSahkoposti() {
        return Sahkoposti;
    }

    public void setSahkoposti(String Sahkoposti) {
        this.Sahkoposti = Sahkoposti;
    }
    @Column
    public String getKatuosoite() {
        return Katuosoite;
    }

    public void setKatuosoite(String Katuosoite) {
        this.Katuosoite = Katuosoite;
    }
    @Column
    public String getPostinumero() {
        return Postinumero;
    }

    public void setPostinumero(String Postinumero) {
        this.Postinumero = Postinumero;
    }

    @Override
    public String toString() {
        return "Tyontekija{" + "TyontekijaId=" + TyontekijaId + ", Etunimi=" + Etunimi + ", Sukunimi=" + Sukunimi + ", Puhelinnumero=" + Puhelinnumero + ", Sahkoposti=" + Sahkoposti + ", Katuosoite=" + Katuosoite + ", Postinumero=" + Postinumero + '}';
    }
    
    
    
    
}

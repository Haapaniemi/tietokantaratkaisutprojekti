/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tuholaistorjuntasovellus;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;
import java.util.Scanner;

/**
 *
 * @author CleverName
 */
public class main {

    static Scanner scan = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int input = 0;
        while (input != 5) {
            print("1. Create");
            print("2. Read");
            print("3. Update");
            print("4. Delete");
            print("5. Quit");

            input = scan.nextInt();

            // CREATE
            if (input == 1) {
                int input2 = 0;
                print("1. Lisää asiakas");
                print("2. Lisää työntekijä");
                print("3. Lisää asiakaskäynti");
                print("4. Lisää tuholainen");

                input2 = scan.nextInt();

                switch (input2) {
                    case 1:
                        lisaaAsiakas();
                        break;
                    case 2:
                        lisaaTyontekija();
                        break;
                    case 3:
                        lisaaAsiakaskaynti();
                        break;
                    case 4:
                        lisaaTuholainen();
                        break;
                    default:
                        break;
                }
                // READ    
            } else if (input == 2) {
                int input2 = 0;
                print("1. Tulosta kaikki asiakkaat");
                print("2. Tulosta kaikki tyontekijat");
                print("3. Tulosta kaikki asiakaskaynnit");
                print("4. Tulosta kaikki tuholaiset");
                print("5. Tulosta tietyn asiakkaan tiedot");
                print("6. Tulosta tietyn työntekijän tiedot");
                print("7. Tulosta tietyn tuholaisen tiedot");
                print("8. Tulosta loytynyyt tuholaiset");

                input2 = scan.nextInt();

                switch (input2) {
                    case 1:
                        tulostaAsiakkaat();
                        break;
                    case 2:
                        tulosta_Tyontekijat();
                        break;
                    case 3:
                        tulostaAsiakaskaynnit();
                        break;
                    case 4:
                        tulostaTuholaiset();
                        break;
                    case 5:
                        tulostaAsiakkaanTiedot();
                        break;
                    case 6:
                        tulostaTyontekijanTiedot();
                        break;
                    case 7:
                        tulostaTuholaisenTiedot();
                        break;
                    case 8:
                        tulostaLoytyneetTuholaiset();
                        break;
                }
            } // UPDATE
            else if (input == 3) {
                int input2 = 0;
                print("1. Päivitä asiakasta");
                print("2. Päivitä työntekijää");
                print("3. Päivitä asiakaskäyntiä");
                print("4. Päivitä tuhollista");

                input2 = scan.nextInt();

                switch (input2) {
                    case 1:
                        paivitaAsiakasta();
                        break;
                    case 2:
                        paivitaTyontekijaa();
                        break;
                    case 3:
                        paivitaAsiakaskayntia();
                        break;
                    case 4:
                        paivitaTuholaista();
                        break;

                }
            } // DELETE
            else if (input == 4) {
                int input2 = 0;
                print("1. Poista asiakas");
                print("2. Poista työntekijä");
                print("3. Poista asiakaskäynti");
                print("4. Poista tuholainen");

                input2 = scan.nextInt();

                switch (input2) {
                    case 1:
                        poistaAsiakas();
                        break;
                    case 2:
                        poistaTyontekija();
                        break;
                    case 3:
                        poistaAsiakaskaynti();
                        break;
                    case 4:
                        poistaTuholainen();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public static void print(String string) {
        System.out.println(string);
    }

    /////////////////////////////////////////////////
    // Persistence funktiot/CRUD-toiminallisuudet///
    ///////////////////////////////////////////////
    private static void lisaaAsiakas() {
        // muuttujat
        String etunimi, sukunimi, puhelinnumero, katuosoite, postinumero, sahkoposti;
        print("Luodaan uusi asiakas.");

        // Pyydetään muuttujien tiedot
        print("Syötä asiakkaan etunimi: ");
        etunimi = scan.next();
        print("Syötä asiakkaan sukunimi; ");
        sukunimi = scan.next();
        print("Syötä asiakkaan puhelinnumero: ");
        puhelinnumero = scan.next();
        print("Syötä asiakkaan katuosoite: ");
        katuosoite = scan.next();
        print("Syötä asiakkaan postinumero: ");
        postinumero = scan.next();
        print("Syötä asiakkaan sahkoposti: ");
        sahkoposti = scan.next();

        // Luodaan uusi olio
        Asiakas asiakas = new Asiakas();
        asiakas.setEtunimi(etunimi);
        asiakas.setSukunimi(sukunimi);
        asiakas.setKatuosoite(katuosoite);
        asiakas.setPostinumero(postinumero);
        asiakas.setPuhelinnumero(puhelinnumero);
        asiakas.setSahkoposti(sahkoposti);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(asiakas);
        em.getTransaction().commit();
        em.close();
    }

    private static void lisaaTyontekija() {
        // muuttujat
        String etunimi, sukunimi, puhelinnumero, katuosoite, postinumero, sahkoposti;
        print("Luodaan uusi tyontekija.");

        // Pyydetään muuttujien tiedot
        print("Syötä tyontekijan etunimi: ");
        etunimi = scan.next();
        print("Syötä tyontekijan sukunimi; ");
        sukunimi = scan.next();
        print("Syötä tyontekijan puhelinnumero: ");
        puhelinnumero = scan.next();
        print("Syötä tyontekijan katuosoite: ");
        katuosoite = scan.next();
        print("Syötä tyontekijan postinumero: ");
        postinumero = scan.next();
        print("Syötä tyontekijan sahkoposti: ");
        sahkoposti = scan.next();

        // Luodaan uusi olio
        Tyontekija tyontekija = new Tyontekija();
        tyontekija.setEtunimi(etunimi);
        tyontekija.setSukunimi(sukunimi);
        tyontekija.setKatuosoite(katuosoite);
        tyontekija.setPostinumero(postinumero);
        tyontekija.setPuhelinnumero(puhelinnumero);
        tyontekija.setSahkoposti(sahkoposti);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(tyontekija);
        em.getTransaction().commit();
        em.close();
    }

    private static void lisaaAsiakaskaynti() {
        // muuttujat
        int AsiakasId, TyontekijaId, TuholaisetId;
        Timestamp Aikaleima;
        String Tila;

        // Muuttujien alustus
        print("Syötä asiakaskaynnin asiakasID");
        AsiakasId = scan.nextInt();
        print("Syötä asiakaskaynnin tyontekijaID");
        TyontekijaId = scan.nextInt();
        print("Syötä asiakaskaynnin tuholaisetID");
        TuholaisetId = scan.nextInt();
        print("Syötä asiakaskaynnin tila");
        Tila = scan.next();
        Aikaleima = new Timestamp(System.currentTimeMillis());

        // Luodaan olio-viittaus
        Asiakaskaynnit asiakaskaynti = new Asiakaskaynnit();
        asiakaskaynti.setAikaleima(Aikaleima);
        asiakaskaynti.setAsiakasId(AsiakasId);
        asiakaskaynti.setTila(Tila);
        asiakaskaynti.setTyontekijaId(TyontekijaId);
        asiakaskaynti.setTuholaisetId(TuholaisetId);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(asiakaskaynti);
        em.getTransaction().commit();
        em.close();
    }

    private static void lisaaTuholainen() {
        // muuttujat
        String nimi, kuvaus, torjunta;

        // Muuttujien alustus
        print("Syötä uuden tuholaisen nimi");
        nimi = scan.next();
        print("Syötä uuden tuholaisen kuvaus");
        kuvaus = scan.next();
        print("Syötä uuden tuholaisen torjunta");
        torjunta = scan.next();

        // Luodaan olio-viittaus
        Tuholainen tuholainen = new Tuholainen();
        tuholainen.setNimi(nimi);
        tuholainen.setKuvaus(kuvaus);
        tuholainen.setTorjunta(torjunta);

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(tuholainen);
        em.getTransaction().commit();
        em.close();
    }

    private static void poistaAsiakas() {
        int id;
        print("Syötä poistettavan kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Asiakas asiakas = em.find(Asiakas.class, id);
        em.remove(asiakas);

        em.getTransaction().commit();
        em.close();
    }

    private static void poistaTyontekija() {
        int id;
        print("Syötä poistettavan kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Tyontekija tyontekija = em.find(Tyontekija.class, id);
        em.remove(tyontekija);

        em.getTransaction().commit();
        em.close();
    }

    private static void poistaAsiakaskaynti() {
        int id;
        print("Syötä poistettavan kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Asiakaskaynnit asiakaskaynti = em.find(Asiakaskaynnit.class, id);
        em.remove(asiakaskaynti);

        em.getTransaction().commit();
        em.close();
    }

    private static void poistaTuholainen() {
        int id;
        print("Syötä poistettavan kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Tuholainen tuholainen = em.find(Tuholainen.class, id);
        em.remove(tuholainen);

        em.getTransaction().commit();
        em.close();
    }

    private static void tulostaAsiakkaat() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        TypedQuery<Asiakas> query = em.createNamedQuery("Asiakas.getAll", Asiakas.class);
        List<Asiakas> results = query.getResultList();
        em.getTransaction().commit();
        em.close();

        for (Asiakas asiakas : results) {
            print(asiakas.toString());
        }
    }

    private static void tulosta_Tyontekijat() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        TypedQuery<Tyontekija> query = em.createNamedQuery("Tyontekija.getAll", Tyontekija.class);
        List<Tyontekija> results = query.getResultList();
        em.getTransaction().commit();
        em.close();

        for (Tyontekija tyontekija : results) {
            print(tyontekija.toString());
        }
    }

    private static void tulostaTuholaiset() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        TypedQuery<Tuholainen> query = em.createNamedQuery("Tuholainen.getAll", Tuholainen.class);
        List<Tuholainen> results = query.getResultList();
        em.getTransaction().commit();
        em.close();

        for (Tuholainen tuholainen : results) {
            print(tuholainen.toString());
        }
    }

    private static void tulostaAsiakaskaynnit() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        TypedQuery<Asiakaskaynnit> query = em.createNamedQuery("Asiakaskaynnit.getAll", Asiakaskaynnit.class);
        List<Asiakaskaynnit> results = query.getResultList();
        em.getTransaction().commit();
        em.close();

        for (Asiakaskaynnit asiakaskaynti : results) {
            print(asiakaskaynti.toString());
        }
    }

    private static void tulostaTuholaisenTiedot() {
        int id;
        print("Syötä tulostuksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Tuholainen tuholainen = em.find(Tuholainen.class, id);
        if (tuholainen != null) {
            tuholainen.toString();
        }

        em.getTransaction().commit();
        em.close();
    }

    private static void tulostaLoytyneetTuholaiset() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        TypedQuery<Loytyneettuholaiset> query = em.createNamedQuery("Loytyneettuholaiset.getAll", Loytyneettuholaiset.class);
        List<Loytyneettuholaiset> results = query.getResultList();
        em.getTransaction().commit();
        em.close();

        for (Loytyneettuholaiset loytyneettuholaiset : results) {
            print(loytyneettuholaiset.toString());
        }
    }

    private static void tulostaTyontekijanTiedot() {
        int id;
        print("Syötä tulostuksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Tyontekija tyontekija = em.find(Tyontekija.class, id);
        if (tyontekija != null) {
            tyontekija.toString();
        }

        em.getTransaction().commit();
        em.close();
    }

    private static void tulostaAsiakkaanTiedot() {
        int id;
        print("Syötä tulostuksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Asiakas asiakas = em.find(Asiakas.class, id);
        if (asiakas != null) {
            asiakas.toString();
        }

        em.getTransaction().commit();
        em.close();
    }

    private static void paivitaAsiakasta() {
        int id;
        print("Syötä päivityksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Asiakas asiakas = em.find(Asiakas.class, id);
        if (asiakas != null) {
            int valinta;
            print("1. Muuta etunimi");
            print("2. Muuta sukunimi");
            print("3. Muuta puhelinnumero");
            print("4. Muuta katuosoite");
            print("5. Muuta postinumero");
            print("6. Muuta sahkoposti");
            valinta = scan.nextInt();
            switch (valinta) {
                case 1:
                    String uusi_etunimi;
                    print("Syötä uusi etunimi: ");
                    uusi_etunimi = scan.next();
                    asiakas.setEtunimi(uusi_etunimi);
                    break;
                case 2:
                    String uusi_sukunimi;
                    print("Syötä uusi sukunimi");
                    uusi_sukunimi = scan.next();
                    asiakas.setSukunimi(uusi_sukunimi);
                    break;
                case 3:
                    String uusi_puhelinnumero;
                    print("Syötä uusi puhelinnumero");
                    uusi_puhelinnumero = scan.next();
                    asiakas.setPuhelinnumero(uusi_puhelinnumero);
                    break;
                case 4:
                    String uusi_katuosoite;
                    print("Syötä uusi katuosoite");
                    uusi_katuosoite = scan.next();
                    asiakas.setKatuosoite(uusi_katuosoite);
                    break;
                case 5:
                    String uusi_postinumero;
                    print("Syötä uusi postinumero");
                    uusi_postinumero = scan.next();
                    asiakas.setPostinumero(uusi_postinumero);
                    break;
                case 6:
                    String uusi_sahkoposti;
                    print("Syötä uusi sahkoposti");
                    uusi_sahkoposti = scan.next();
                    asiakas.setSahkoposti(uusi_sahkoposti);
                    break;
            }
        }

        em.getTransaction().commit();
        em.close();
    }

    private static void paivitaTyontekijaa() {
        int id;
        print("Syötä päivityksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Tyontekija tyontekija = em.find(Tyontekija.class, id);
        if (tyontekija != null) {
            int valinta;
            print("1. Muuta etunimi");
            print("2. Muuta sukunimi");
            print("3. Muuta puhelinnumero");
            print("4. Muuta katuosoite");
            print("5. Muuta postinumero");
            print("6. Muuta sahkoposti");
            valinta = scan.nextInt();
            switch (valinta) {
                case 1:
                    String uusi_etunimi;
                    print("Syötä uusi etunimi: ");
                    uusi_etunimi = scan.next();
                    tyontekija.setEtunimi(uusi_etunimi);
                    break;
                case 2:
                    String uusi_sukunimi;
                    print("Syötä uusi sukunimi");
                    uusi_sukunimi = scan.next();
                    tyontekija.setSukunimi(uusi_sukunimi);
                    break;
                case 3:
                    String uusi_puhelinnumero;
                    print("Syötä uusi puhelinnumero");
                    uusi_puhelinnumero = scan.next();
                    tyontekija.setPuhelinnumero(uusi_puhelinnumero);
                    break;
                case 4:
                    String uusi_katuosoite;
                    print("Syötä uusi katuosoite");
                    uusi_katuosoite = scan.next();
                    tyontekija.setKatuosoite(uusi_katuosoite);
                    break;
                case 5:
                    String uusi_postinumero;
                    print("Syötä uusi postinumero");
                    uusi_postinumero = scan.next();
                    tyontekija.setPostinumero(uusi_postinumero);
                    break;
                case 6:
                    String uusi_sahkoposti;
                    print("Syötä uusi sahkoposti");
                    uusi_sahkoposti = scan.next();
                    tyontekija.setSahkoposti(uusi_sahkoposti);
                    break;
            }
        }

        em.getTransaction().commit();
        em.close();
    }

    private static void paivitaAsiakaskayntia() {
        int id;
        print("Syötä päivityksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Asiakaskaynnit asiakaskaynti = em.find(Asiakaskaynnit.class, id);
        if (asiakaskaynti != null) {
            int valinta;
            print("1. Muuta Aikaleima");
            print("2. Muuta Tila");
            valinta = scan.nextInt();
            switch (valinta) {
                case 1:
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    asiakaskaynti.setAikaleima(timestamp);
                    break;
                case 2:
                    String uusi_tila;
                    print("Syötä uusi tila");
                    uusi_tila = scan.next();
                    asiakaskaynti.setTila(uusi_tila);
                    break;
            }
        }

        em.getTransaction().commit();
        em.close();
    }

    private static void paivitaTuholaista() {
        int id;
        print("Syötä päivityksen kohteen id: ");
        id = scan.nextInt();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TuholaistorjuntaSovellusPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Tuholainen tuholainen = em.find(Tuholainen.class, id);
        if (tuholainen != null) {
            int valinta;
            print("1. Muuta nimi");
            print("2. Muuta kuvaus");
            print("3. Muuta torjunta");
            valinta = scan.nextInt();
            switch (valinta) {
                case 1:
                    String uusi_nimi;
                    print("Syötä uusi nimi");
                    uusi_nimi = scan.next();
                    tuholainen.setNimi(uusi_nimi);
                    break;
                case 2:
                    String uusi_kuvaus;
                    print("Syötä uusi kuvaus");
                    uusi_kuvaus = scan.next();
                    tuholainen.setKuvaus(uusi_kuvaus);
                    break;
                case 3:
                    String uusi_torjunta;
                    print("Syötä uusi torjunta");
                    uusi_torjunta = scan.next();
                    tuholainen.setTorjunta(uusi_torjunta);
                    break;
            }
        }

        em.getTransaction().commit();
        em.close();

    }

}

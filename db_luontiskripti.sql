DROP TABLE IF EXISTS ASIAKAS;
DROP TABLE IF EXISTS TUHOLAINEN;
DROP TABLE IF EXISTS LOYTYNEETTUHOLAISET;
DROP TABLE IF EXISTS TYONTEKIJA;
DROP TABLE IF EXISTS ASIAKASKAYNNIT;

CREATE TABLE ASIAKAS
(
  AsiakasID INT NOT NULL AUTO_INCREMENT,
  Etunimi VARCHAR(40) NOT NULL,
  Sukunimi VARCHAR(40) NOT NULL,
  Puhelinnumero VARCHAR(16),
  Katuosoite VARCHAR(40),
  Postinumero VARCHAR(5),
  Sahkoposti VARCHAR(60),
  PRIMARY KEY (AsiakasID)
);

CREATE TABLE TUHOLAINEN
(
  TuholainenID INT NOT NULL AUTO_INCREMENT,
  Nimi VARCHAR(40) NOT NULL,
  Kuvaus VARCHAR(255) NOT NULL,
  Torjunta VARCHAR(255) NOT NULL,
  PRIMARY KEY (TuholainenID)
);

CREATE TABLE LOYTYNEETTUHOLAISET
(
  TuholaisetID INT NOT NULL AUTO_INCREMENT,
  TuholainenID INT NOT NULL,
  PRIMARY KEY (TuholaisetID),
  FOREIGN KEY (TuholainenID) REFERENCES TUHOLAINEN (TuholainenID) ON DELETE CASCADE
);

CREATE TABLE TYONTEKIJA
(
  TyontekijaID INT NOT NULL AUTO_INCREMENT,
  Etunimi VARCHAR(40) NOT NULL,
  Sukunimi VARCHAR(40) NOT NULL,
  Puhelinnumero VARCHAR(16) NOT NULL,
  Sahkoposti VARCHAR(60) NOT NULL,
  Katuosoite VARCHAR(40) NOT NULL,
  Postinumero VARCHAR(5) NOT NULL,
  PRIMARY KEY (TyontekijaID)
);

CREATE TABLE ASIAKASKAYNNIT
(
  AsiakaskayntiID INT NOT NULL AUTO_INCREMENT,
  AsiakasID INT NOT NULL,
  TyontekijaID INT NOT NULL,
  Aikaleima TIMESTAMP NOT NULL,
  Tila VARCHAR(80) NOT NULL,
  TuholaisetID INT NOT NULL,
  PRIMARY KEY (AsiakaskayntiID),
  FOREIGN KEY (AsiakasID) REFERENCES ASIAKAS (AsiakasID) ON DELETE CASCADE,
  FOREIGN KEY (TyontekijaID) REFERENCES TYONTEKIJA (TyontekijaID) ON DELETE CASCADE,
  FOREIGN KEY (TuholaisetID) REFERENCES LOYTYNEETTUHOLAISET (TuholaisetID) ON DELETE CASCADE
);

INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (1,"Pasi","Heikkinen","044-516123","Bulevardi 31","00180","pasi.heikkinen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (2,"Anssi","Korhonen","044-123677","Tahtikuja 1","00100","anssi.korhonen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (3,"Kalevi","Makinen","040-899564","Mannerheimintie 1","00210","kalevi.makinen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (4,"Olavi","Virtanen","040-455663","Koodikuja 10","00200","olavi.virtanen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (5,"Tapani","Koskinen","044-102344","Mutkatie 1a","00190","tapani.koskinen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (6,"Anna","Nieminen","044-350012","Kampintori 22b","00110","anna.nieminen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (7,"Johanna","Laine","040-116153","Kauppatie 24c","00140","johanna.laine@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (8,"Liisa","Hamalainen","040-526103","Niemenkuja 1d","00220","liisa.hamalainen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (9,"Antero","Virtanen","044-516000","Mustikkatie 2","00400","antero.virtanen@gmail.com");
INSERT INTO ASIAKAS (AsiakasID,Etunimi,Sukunimi,Puhelinnumero,Katuosoite,Postinumero,sahkoposti) VALUES (10,"Juhani","Heikkinen","044-500123","Bulevardi 31","00180","juhani.heikkinen@gmail.com");

INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (1,"Mikael","Heikkinen","040-145890","mikael.heikkinen@gmail.com","Baarikuja 1","00110");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (2,"Eetu","Korhonen","044-122810","eetu.korhonen@gmail.com","Kauppakuja 10","00210");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (3,"Helena","Jarvinen","040-765234","helena.jarvinen@gmail.com","Mantytie 8","00234");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (4,"Jussi","Laine","044-145455","jussi.laine@gmail.com","Baarikuja 1","00110");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (5,"Toni","Halonen","040-122810","mikael.heikkinen@gmail.com","Mestaritie 2","00120");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (6,"Anneli","Torvi","040-655321","anneli.torvi@gmail.com","Maalarinkuja 11","00210");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (7,"Mika","Hakkinen","040-145455","mika.hakkinen@gmail.com","Westendinkuja 23","00260");
INSERT INTO TYONTEKIJA (TyontekijaID,Etunimi,Sukunimi,Puhelinnumero,Sahkoposti,Katuosoite,Postinumero) VALUES (8,"Matti","Metsa","040-145353","matti.metsa@gmail.com","Metsatie 3","00220");

INSERT INTO TUHOLAINEN (TuholainenID,Nimi,Kuvaus,Torjunta) VALUES (1,"Torakka","Ruskea, kellert�v�. Leve� ja litte� ruumis, noin 2-5cm pitk�.","Hy�nteissirote");
INSERT INTO TUHOLAINEN (TuholainenID,Nimi,Kuvaus,Torjunta) VALUES (2,"Banaanikarpanen","2-4mm pitk�, puna- tai mustanruskea. Nopea lis��ntymistahti.","Raid k�rp�spyydys");
INSERT INTO TUHOLAINEN (TuholainenID,Nimi,Kuvaus,Torjunta) VALUES (3,"Lutikka","8mm pitk�, leve� ja litte�, ruskea.","Baygon torjunta-aerosoli, vuodevaatteiden pesu 60�C.");
INSERT INTO TUHOLAINEN (TuholainenID,Nimi,Kuvaus,Torjunta) VALUES (4,"Faaraomuurahainen","2mm pitk�, kellert�v� tai punainen, kaikkiruokainen","Raid muurahais aerosoli");
INSERT INTO TUHOLAINEN (TuholainenID,Nimi,Kuvaus,Torjunta) VALUES (5,"Sokeritoukka", "1cm pitk�, hopeahohtoiset suomut, el�� kosteissa tiloissa mm. kylpyhuone","Raid aerosoli");

INSERT INTO LOYTYNEETTUHOLAISET (TuholaisetID,TuholainenID) VALUES (1,1);
INSERT INTO LOYTYNEETTUHOLAISET (TuholaisetID,TuholainenID) VALUES (2,4);
INSERT INTO LOYTYNEETTUHOLAISET (TuholaisetID,TuholainenID) VALUES (3,5);
INSERT INTO LOYTYNEETTUHOLAISET (TuholaisetID,TuholainenID) VALUES (4,2);
INSERT INTO LOYTYNEETTUHOLAISET (TuholaisetID,TuholainenID) VALUES (5,3);

INSERT INTO ASIAKASKAYNNIT (AsiakaskayntiID,AsiakasID,TyontekijaID,Aikaleima,Tila,TuholaisetID) VALUES (1,1,1,NOW(),"Tyon alla",1);
INSERT INTO ASIAKASKAYNNIT (AsiakaskayntiID,AsiakasID,TyontekijaID,Aikaleima,Tila,TuholaisetID) VALUES (2,2,1,NOW(),"Valmis",2);
INSERT INTO ASIAKASKAYNNIT (AsiakaskayntiID,AsiakasID,TyontekijaID,Aikaleima,Tila,TuholaisetID) VALUES (3,4,6,NOW(),"Tyon alla",3);
INSERT INTO ASIAKASKAYNNIT (AsiakaskayntiID,AsiakasID,TyontekijaID,Aikaleima,Tila,TuholaisetID) VALUES (4,10,2,NOW(),"Valmis",4);
INSERT INTO ASIAKASKAYNNIT (AsiakaskayntiID,AsiakasID,TyontekijaID,Aikaleima,Tila,TuholaisetID) VALUES (5,8,3,NOW(),"Tyon alla",5);